# Read me

 - `yarn` or `npm i` to install all the things.
 - `yarn dev` or `npm run dev` will run the dev server.
 - `yarn build` or `npm run build` to build a universal app in the `.next` directory.
 - `yarn start` or `npm run start` to run the app in production mode from the `.next` directory.