const merge = require('lodash.merge');
const withSass = require('@zeit/next-sass');
const webpackConfig = require('./webpack.config.js');
const withSvgr = require("next-svgr");

module.exports = withSvgr(withSass({
  sassLoaderOptions: {
    importLoaders: 1,
    includePaths: ['/Users/jh45/Documents/code/getbusy/styles/variables.scss'],
  },
  cssModules: true,
  pageExtensions: ['js', 'jsx', 'md', 'mdx'],
  webpack: (nextConfig) => {
    const config = { ...nextConfig };

    return merge(config, webpackConfig);
  },
}));
