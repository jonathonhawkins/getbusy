import React, { useState } from 'react';
import classnames from 'classnames';

import styles from "./style.scss"

import SideNav from '~/components/SideNav';
import Hamburger from '~/components/Hamburger';
import Logo from '~/static/logo.svg';

const Header = () => {
  const [isSideNavVisible, setSideNavVisible] = useState(false);
  const toggleSideNav = () => setSideNavVisible(!isSideNavVisible);

  return (
    <>
      <div className={styles.headerBackplate} />
      <header className={styles.header}>
        <Logo
          className={classnames(
            styles.logo,
            (isSideNavVisible) ? styles.invert : '',
          )}
        />
        <Hamburger
          className={classnames(
            styles.action,
            (isSideNavVisible) ? styles.invert : '',
          )}
          clickHandler={toggleSideNav}
          isActive={isSideNavVisible}
        />
        <SideNav isVisible={isSideNavVisible}/>
      </header>
    </>
  )
};


export default Header;
