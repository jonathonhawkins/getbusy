import React from 'react';
import classnames from 'classnames';
import styles from "./style.scss"

const SideNav = ({ isVisible }) => {
  const visibility = (isVisible)
    ? styles.show
    : '';
  return (
    <nav className={classnames(styles.sideNav, visibility)}>
      <ul>
        <li>
          Blog
        </li>
        <li>
          About Us
        </li>
        <li>
          Investors
        </li>
        <li>
          Media
        </li>
      </ul>
    </nav>
  )
};


export default SideNav;
