import React, { useReducer, useEffect } from 'react';
import classnames from 'classnames';

import styles from "./style.scss"
import Hamburger from '~/components/Hamburger';

const reducer = (_, action) => {
  switch (action.type) {
    case 'loading':
    case 'loaded':
      return { ...action.payload };
  }
}

const LazyImage = ({ small, large, width, height, setSelection }) => {
  const [state, dispatch] = useReducer(reducer, {
    sauce: undefined,
    isLoading: undefined,
    width: undefined,
    height: undefined,
  });

  useEffect(() => {
    dispatch({
      type: 'loading',
      payload: {
        sauce: small,
        isLoading: true,
        width,
        height,
      },
    })
    const img = document.createElement('img');
    img.src = large;
    img.onload = function () {
      dispatch({
        type: 'loading',
        payload: {
          sauce: img.src,
          isLoading: false,
          width: img.width,
          height: img.height,
        },
      })
    };
  }, [small, large])

  const loading = (state.isLoading)
    ? null
    : styles.loaded;
  return (
    <img
      className={classnames(styles.image, loading)}
      src={state.sauce}
      style={{
        width: `${state.width}px`,
        height: `${state.height}px`,
      }}
      onClick={() => setSelection(undefined, true, false)}
    />
  );
}

const Modal = ({ selection = {}, setSelection, isVisible, hide }) => {
  const {
    urls = {},
    width = 100,
    height = 100,
  } = selection;
  const large = urls.regular || urls.raw || urls.full;
  const visible = (isVisible)
    ? styles.show
    : styles.hide;
  return (
    <>
      <div className={classnames(styles.background, visible)} />
      <div className={classnames(styles.modal, visible)}>
        <div className={styles.imageContainer}>
          <LazyImage
            width={width}
            height={height}
            small={urls.small}
            large={large}
            setSelection={setSelection}
          />
        </div>
        <div className={styles.burgerWapping}>
          <Hamburger
            isActive={true}
            style={{ backgroundColor: 'white' }}
            clickHandler={hide}
          />
        </div>
      </div>
    </>
  );
};

export default Modal;
