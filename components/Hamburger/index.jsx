import React from 'react';
import classnames from 'classnames';

import styles from "./style.scss"


const Hamburger = ({ isActive, clickHandler, style = {} }) => {
  const active = (isActive)
    ? styles.active
    : undefined;
  return (
    <div
      className={classnames(styles.hamburger, active)}
      onClick={clickHandler}
    >
      <span style={style}/>
      <span style={style}/>
      <span style={style}/>
    </div>
  )
}



export default Hamburger;
