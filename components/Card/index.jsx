import React from 'react';
import styles from "./style.scss"

const Card = ({ user = {}, urls = {}, setSelection }) => (
  <div
    onClick={setSelection}
    className={styles.card}
  >
    <div className={styles.thumbnail} style={{
      backgroundImage: `url(${urls.small})`,
    }}/>
    <div className={styles.footer}>
      <div
        className={styles.profileImage}
        style={{
          backgroundImage: `url(${user.profile_image.medium})`,
        }}
      />
      <p className={styles.name}>{user.name}</p>
    </div>
  </div>
);

export default Card;
