import React, { useReducer } from 'react';
import fetch from 'isomorphic-unfetch';

import styles from "./styles.scss"

import Header from '~/components/Header';
import Modal from '~/components/Modal';
import Card from '~/components/Card';

const reducer = (state, action) => {
  switch (action.type) {
    case 'selection':
      console.log(action);
      return { ...action.payload };
    case 'hideModal':
      return { ...state, ...action.payload };
  }
}

const Home = ({ images = [] }) => {
  const [state, dispatch] = useReducer(reducer, {
    selection: 0,
    isVisible: false,
  });
  console.log(state.selection);

  const max = images.length - 1;
  const selectionHandler = (index = state.selection, isNext, isPrev) => {
    if (isNext) {
      index += 1;
    } else if (isPrev) {
      index -= 1;
    }
    if (index > max) {
      index = 0;
    } else if (index < 0) {
      index = max;
    }
    dispatch({
      type: 'selection',
      payload: {
        selection: index,
        isVisible: true,
      },
    });
  };
  const hideModal = () => dispatch({
    type: 'hideModal',
    payload: {
      isVisible: false,
    },
  })

  return (
    <>
      <Header />
      <Modal
        selection={images[state.selection]}
        setSelection={selectionHandler}
        isVisible={state.isVisible}
        hide={hideModal}
      />
      <div className={styles.container}>
        {
          images.map((image, index) => (
            <Card
              key={`image-card-${index}`}
              {...image}
              setSelection={() => selectionHandler(index)}
            />
          ))
        }
      </div>
    </>
  )
};

Home.getInitialProps = async () => {
  const response = await fetch('http://localhost:3000/api/images/list');
  if (response.status !== 200) {
    console.error('Failed to load images');
  }
  const images = await response.json();
  return { images };
};

export default Home;
