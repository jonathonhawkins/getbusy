import '~/styles/variables.scss';
import '~/styles/root.scss';

import App, { Container } from 'next/app';

import Head from 'next/head';
import React from 'react';

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <Head>
          <meta charSet="UTF-8" />
          <title>GetBusy Frontend</title>
          <meta name="author" content="Jonathon Hawkins" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link
            rel="shortcut icon"
            type="image/png"
            href="/static/favicon.png"
            sizes="32x32"
          />
          <link
            rel="shortcut icon"
            type="image/png"
            href="/static/favicon.png"
            sizes="16x16"
          />
        </Head>
        <Component {...pageProps}/>
      </Container>
    );
  }
}

export default MyApp;
