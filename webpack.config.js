const path = require('path');

module.exports = {
  node: {
    fs: 'empty',
  },
  resolve: {
    alias: {
      '~': path.resolve(__dirname),
    },
    extensions: ['.js', '.jsx'],
  },
  // module: {
  //   rules: [
  //     {
  //       test: /\.svg$/,
  //       use: ['@svgr/webpack', 'url-loader'],
  //     },
  //   ],
  // },
};
